__author__ = 'pierrestarkov'

class DataContainer:

    def __init__(self):
        self.stockList = None
        self.stockSymbol = None
        self.stockData = None
        self.attributes = None
        self.classes = None

    def getDayMin(self):
        return 0

    def getDayMax(self):
        return len(self.stockData)

    def getTimeMin(self):
        return 90

    def getTimeMax(self,day):
        return len(self.stockData[day][0])-50