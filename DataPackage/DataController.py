# coding=utf-8
# from datetime import datetime
from numpy import array, concatenate, insert, vstack, hstack
from DataReader import DataReader
from DataContainer import DataContainer
from Extra.Path import Path
from TaLibPackage.TaLibAttributsClassesController import TaLibVectorAttributsController



##################################################################################


class DataController():

    def __init__(self):
        self.path               = Path()
        self.dataReader         = DataReader()
        self.dataContainer      = DataContainer()
        self.talibController    = TaLibVectorAttributsController()
        self.dataContainer.stockList    = self.dataReader.readCompaniesList(fileName=self.path.stocksListFile)

    def setStockData(self,stockIndex):
        self.dataContainer.stockSymbol  = self.dataContainer.stockList[stockIndex]
        self.dataContainer.stockData    = self.dataReader.readCompanyDataPerDay(self.path.stocksCsvDir+self.dataContainer.stockSymbol)

    def getAttributesDayMatrix(self, day):
        attributesDayMatrix = self.talibController.getAttributesVector(data=self.dataContainer.stockData, day=day, time=self.dataContainer.getTimeMin())
        for time in xrange(self.dataContainer.getTimeMin()+1, self.dataContainer.getTimeMax(day=day)):
            attributesDayMatrix = vstack((attributesDayMatrix, self.talibController.getAttributesVector(data=self.dataContainer.stockData, day=day, time=time)))
        return attributesDayMatrix

    def getClassesDayVector(self, day, futurDistance):
        classesDayVector = self.talibController.getClassScalar(data=self.dataContainer.stockData, day=day, time=self.dataContainer.getTimeMin(), futurDistance=futurDistance)
        for time in xrange(self.dataContainer.getTimeMin()+1, self.dataContainer.getTimeMax(day)):
            classesDayVector = hstack((classesDayVector, self.talibController.getClassScalar(data=self.dataContainer.stockData, day=day, time=time, futurDistance=futurDistance)))
        return classesDayVector



    def getAttributesTotalMatrix(self):
        attributesTotalMatrix = self.getAttributesDayMatrix(day=self.dataContainer.getDayMin())
        for day in xrange(self.dataContainer.getDayMin(), self.dataContainer.getDayMax()):
            attributesTotalMatrix = vstack((attributesTotalMatrix, self.getAttributesDayMatrix(day=day)))
        self.dataContainer.attributes = attributesTotalMatrix

    def getClassesTotalVector(self,futurDistance):
        classesTotalVector = self.getClassesDayVector(day=self.dataContainer.getDayMin(), futurDistance=futurDistance)
        for day in xrange(self.dataContainer.getDayMin(), self.dataContainer.getDayMax()):
            classesTotalVector = hstack((classesTotalVector, self.getClassesDayVector(day=day, futurDistance=futurDistance)))
        self.dataContainer.classes = classesTotalVector
