__author__ = 'pierrestarkov'

from enum import Enum

class DataEnum(Enum):
    date = 0
    time = 1
    open_ = 2
    high = 3
    low = 4
    close = 5
    volume = 6
    DATE_FMT = '%Y-%m-%d'
    TIME_FMT = '%H:%M:%S'