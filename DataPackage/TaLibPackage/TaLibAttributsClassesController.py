__author__ = 'pierrestarkov'
from TaLibIndicatorController import TaLibScalarAttributsCreator
from numpy import array, insert
# from multiprocessing.dummy import Pool
# from multiprocessing.pool import ApplyResult
#
#
# def _pickle_method(method):
#     func_name = method.im_func.__name__
#     obj = method.im_self
#     cls = method.im_class
#     return _unpickle_method, (func_name, obj, cls)
#
# def _unpickle_method(func_name, obj, cls):
#     for cls in cls.mro():
#         try:
#             func = cls.__dict__[func_name]
#         except KeyError:
#             pass
#         else:
#             break
#     return func.__get__(obj, cls)

class TaLibVectorAttributsController():

    def __init__(self):
        self.taLibVectorCreator = TaLibScalarAttributsCreator()

    # def getAttributesVector(self,data,day,time):
    #     pool1 = Pool(8)
    #     pool2 = Pool(8)
    #     async_results1 = [ pool1.apply_async(self.get40Input, (i,)) for i in time ]
    #     async_results2 = [ pool2.apply_async(self.getOutput, (i,)) for i in time ]
    #     map(ApplyResult.wait, async_results1)
    #     map(ApplyResult.wait, async_results2)
    #     input_=[r.get() for r in async_results1]
    #     output_=[r.get() for r in async_results2]
    #     #
    #     self.attributs[h:h+len(time),:] = input_
    #     self.classes[h:h+len(time)] = output_

    def getAttributesVector(self, data, day, time):
        attributesVector = array([])
        if (time < 90):
            print "ERROR! TIME SHOULD BE GREATER THAN 90 !!"
        else:
            attributesVector = insert(attributesVector, len(attributesVector), self.taLibVectorCreator.getADOSCpasttime(data=data,day=day,time=time))
            attributesVector = insert(attributesVector, len(attributesVector), self.taLibVectorCreator.getADXRpasttime(data=data,day=day,time=time))
            attributesVector = insert(attributesVector, len(attributesVector), self.taLibVectorCreator.getADXpasttime(data=data,day=day,time=time))
            attributesVector = insert(attributesVector, len(attributesVector), self.taLibVectorCreator.getADpasttime(data=data,day=day,time=time))
            attributesVector = insert(attributesVector, len(attributesVector), self.taLibVectorCreator.getAPOpasttime(data=data,day=day,time=time))
            attributesVector = insert(attributesVector, len(attributesVector), self.taLibVectorCreator.getAROONOSCpasttime(data=data,day=day,time=time))
            attributesVector = insert(attributesVector, len(attributesVector), self.taLibVectorCreator.getATRpasttime(data=data,day=day,time=time))
            attributesVector = insert(attributesVector, len(attributesVector), self.taLibVectorCreator.getBBANDSUPPER(data=data,day=day,time=time))
            attributesVector = insert(attributesVector, len(attributesVector), self.taLibVectorCreator.getBBANDSMIDDLE(data=data,day=day,time=time))
            attributesVector = insert(attributesVector, len(attributesVector), self.taLibVectorCreator.getBBANDSLOWER(data=data,day=day,time=time))
            attributesVector = insert(attributesVector, len(attributesVector), self.taLibVectorCreator.getBOPpasttime(data=data,day=day,time=time))
            attributesVector = insert(attributesVector, len(attributesVector), self.taLibVectorCreator.getCCIpasttime(data=data,day=day,time=time))
            attributesVector = insert(attributesVector, len(attributesVector), self.taLibVectorCreator.getCMOpasttime(data=data,day=day,time=time))
            attributesVector = insert(attributesVector, len(attributesVector), self.taLibVectorCreator.getDEMApasttime(data=data,day=day,time=time))
            attributesVector = insert(attributesVector, len(attributesVector), self.taLibVectorCreator.getDXpasttime(data=data,day=day,time=time))
            attributesVector = insert(attributesVector, len(attributesVector), self.taLibVectorCreator.getDerivePremiere(data=data,day=day,time=time))
            attributesVector = insert(attributesVector, len(attributesVector), self.taLibVectorCreator.getDeriveSeconde(data=data,day=day,time=time))
            attributesVector = insert(attributesVector, len(attributesVector), self.taLibVectorCreator.getEMAP(data=data,day=day,time=time))
            attributesVector = insert(attributesVector, len(attributesVector), self.taLibVectorCreator.getEMAPpasttime(data=data,day=day,time=time))
            attributesVector = insert(attributesVector, len(attributesVector), self.taLibVectorCreator.getEMAV(data=data,day=day,time=time))
            attributesVector = insert(attributesVector, len(attributesVector), self.taLibVectorCreator.getEMAVpasttime(data=data,day=day,time=time))
            attributesVector = insert(attributesVector, len(attributesVector), self.taLibVectorCreator.getHT_DCPERIODpasttime(data=data,day=day,time=time))
            attributesVector = insert(attributesVector, len(attributesVector), self.taLibVectorCreator.getHT_DCPHASEpasttime(data=data,day=day,time=time))
            attributesVector = insert(attributesVector, len(attributesVector), self.taLibVectorCreator.getHT_PHASORpasttime(data=data,day=day,time=time))
            attributesVector = insert(attributesVector, len(attributesVector), self.taLibVectorCreator.getHT_SINEpasttime(data=data,day=day,time=time))
            attributesVector = insert(attributesVector, len(attributesVector), self.taLibVectorCreator.getHT_TRENDLINEpasttime(data=data,day=day,time=time))
            attributesVector = insert(attributesVector, len(attributesVector), self.taLibVectorCreator.getMACD(data=data,day=day,time=time))
            attributesVector = insert(attributesVector, len(attributesVector), self.taLibVectorCreator.getMACDSIGNAL(data=data,day=day,time=time))
            attributesVector = insert(attributesVector, len(attributesVector), self.taLibVectorCreator.getMACDHIST(data=data,day=day,time=time))
            attributesVector = insert(attributesVector, len(attributesVector), self.taLibVectorCreator.getMApasttime(data=data,day=day,time=time))
            attributesVector = insert(attributesVector, len(attributesVector), self.taLibVectorCreator.getMFIpasttime(data=data,day=day,time=time))
            attributesVector = insert(attributesVector, len(attributesVector), self.taLibVectorCreator.getMIDPOINTpasttime(data=data,day=day,time=time))
            attributesVector = insert(attributesVector, len(attributesVector), self.taLibVectorCreator.getMIDPRICEpasttime(data=data,day=day,time=time))
            attributesVector = insert(attributesVector, len(attributesVector), self.taLibVectorCreator.getMINUS_DIpasttime(data=data,day=day,time=time))
            attributesVector = insert(attributesVector, len(attributesVector), self.taLibVectorCreator.getMINUS_DMpasttime(data=data,day=day,time=time))
            attributesVector = insert(attributesVector, len(attributesVector), self.taLibVectorCreator.getMOMpasttime(data=data,day=day,time=time))
            attributesVector = insert(attributesVector, len(attributesVector), self.taLibVectorCreator.getNATRpasttime(data=data,day=day,time=time))
            attributesVector = insert(attributesVector, len(attributesVector), self.taLibVectorCreator.getPLUS_DIpasttime(data=data,day=day,time=time))
            attributesVector = insert(attributesVector, len(attributesVector), self.taLibVectorCreator.getPLUS_DMpasttime(data=data,day=day,time=time))
            attributesVector = insert(attributesVector, len(attributesVector), self.taLibVectorCreator.getPPOpasttime(data=data,day=day,time=time))
            attributesVector = insert(attributesVector, len(attributesVector), self.taLibVectorCreator.getPRICE(data=data,day=day,time=time))
            attributesVector = insert(attributesVector, len(attributesVector), self.taLibVectorCreator.getPRICE(data=data,day=day,time=time-1))
            attributesVector = insert(attributesVector, len(attributesVector), self.taLibVectorCreator.getPRICE(data=data,day=day,time=time-2))
            attributesVector = insert(attributesVector, len(attributesVector), self.taLibVectorCreator.getPRICE(data=data,day=day,time=time-3))
            attributesVector = insert(attributesVector, len(attributesVector), self.taLibVectorCreator.getPRICE(data=data,day=day,time=time-4))
            attributesVector = insert(attributesVector, len(attributesVector), self.taLibVectorCreator.getPRICE(data=data,day=day,time=time-5))
            attributesVector = insert(attributesVector, len(attributesVector), self.taLibVectorCreator.getPRICE(data=data,day=day,time=time-6))
            attributesVector = insert(attributesVector, len(attributesVector), self.taLibVectorCreator.getPRICE(data=data,day=day,time=time-7))
            attributesVector = insert(attributesVector, len(attributesVector), self.taLibVectorCreator.getPRICE(data=data,day=day,time=time-8))
            attributesVector = insert(attributesVector, len(attributesVector), self.taLibVectorCreator.getROCPpasttime(data=data,day=day,time=time))
            attributesVector = insert(attributesVector, len(attributesVector), self.taLibVectorCreator.getROCR100pasttime(data=data,day=day,time=time))
            attributesVector = insert(attributesVector, len(attributesVector), self.taLibVectorCreator.getROCRpasttime(data=data,day=day,time=time))
            attributesVector = insert(attributesVector, len(attributesVector), self.taLibVectorCreator.getROCpasttime(data=data,day=day,time=time))
            attributesVector = insert(attributesVector, len(attributesVector), self.taLibVectorCreator.getRSIpasttime(data=data,day=day,time=time))
            attributesVector = insert(attributesVector, len(attributesVector), self.taLibVectorCreator.getSAREXTpasttime(data=data,day=day,time=time))
            attributesVector = insert(attributesVector, len(attributesVector), self.taLibVectorCreator.getSARpasttime(data=data,day=day,time=time))
            attributesVector = insert(attributesVector, len(attributesVector), self.taLibVectorCreator.getSMAK(data=data,day=day,time=time))
            attributesVector = insert(attributesVector, len(attributesVector), self.taLibVectorCreator.getSMAKpasttime(data=data,day=day,time=time))
            attributesVector = insert(attributesVector, len(attributesVector), self.taLibVectorCreator.getSTOCH(data=data,day=day,time=time))
            attributesVector = insert(attributesVector, len(attributesVector), self.taLibVectorCreator.getSTOCHpasttime(data=data,day=day,time=time))
            attributesVector = insert(attributesVector, len(attributesVector), self.taLibVectorCreator.getT3pasttime(data=data,day=day,time=time))
            attributesVector = insert(attributesVector, len(attributesVector), self.taLibVectorCreator.getTEMApasttime(data=data,day=day,time=time))
            attributesVector = insert(attributesVector, len(attributesVector), self.taLibVectorCreator.getTRANGEpasttime(data=data,day=day,time=time))
            attributesVector = insert(attributesVector, len(attributesVector), self.taLibVectorCreator.getTRIMApasttime(data=data,day=day,time=time))
            attributesVector = insert(attributesVector, len(attributesVector), self.taLibVectorCreator.getTRIXpasttime(data=data,day=day,time=time))
            attributesVector = insert(attributesVector, len(attributesVector), self.taLibVectorCreator.getULTOSCpasttime(data=data,day=day,time=time))
            attributesVector = insert(attributesVector, len(attributesVector), self.taLibVectorCreator.getVOLUME(data=data,day=day,time=time))
            attributesVector = insert(attributesVector, len(attributesVector), self.taLibVectorCreator.getWILLRpasttime(data=data,day=day,time=time))
        return attributesVector

    def getClassScalar(self, data, day, time, futurDistance):
        # return self.taLibVectorCreator.getFuturMaxValue(data=data, day=day, time=time, futurDistance=futurDistance)
        # return self.taLibVectorCreator.getFuturPrice(data=data, day=day, time=time, futurDistance=futurDistance)
        return self.taLibVectorCreator.getFuturMeanPrice(data=data, day=day, time=time, futurDistance=futurDistance)
