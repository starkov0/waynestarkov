# coding=utf-8
import talib
from numpy import concatenate, array, invert, isnan, mean
from DataPackage.DataEnum import DataEnum


##################################################################################

class TaLibScalarAttributsCreator():

    # deletes NaN values in a vector
    def deleteNanValues(self,vect):
        return vect[invert(isnan(vect))]


##################################################################################
    # get indicators -> FUTUR
    def getFuturMaxValue(self, data, day, time, futurDistance):
        maxValue = max(data[day][DataEnum.close][time+1:time+1+futurDistance])
        currentValue = data[day][DataEnum.close][time]
        result = maxValue / currentValue
        return [result]

    def getFuturMeanPrice(self, data, day, time, futurDistance):
        values = data[day][DataEnum.close][time+1:time+1+futurDistance]
        meanValue = mean(values)
        return [meanValue]

    def getFuturPrice(self, data, day, time, futurDistance):
        price = data[day][DataEnum.close][time+futurDistance]
        return [price]

    ##################################################################################
    # indicators -> SBN
    def getMACD(self, data, day, time):
        macd, macdsignal, macdhist = talib.MACD(array(data[day][DataEnum.close][time - (33 + 3):time], dtype=float))
        return self.deleteNanValues(macd)

    def getMACDSIGNAL(self, data, day, time):
        macd, macdsignal, macdhist = talib.MACD(array(data[day][DataEnum.close][time - (33 + 3):time], dtype=float))
        return self.deleteNanValues(macdsignal)

    def getMACDHIST(self, data, day, time):
        macd, macdsignal, macdhist = talib.MACD(array(data[day][DataEnum.close][time - (33 + 3):time], dtype=float))
        return self.deleteNanValues(macdhist)

    def getEMAV(self, data, day, time):
        emav = talib.EMA(array(data[day][DataEnum.volume][time - (29 + 1):time], dtype=float))
        return self.deleteNanValues(emav)

    def getEMAP(self, data, day, time):
        emap = talib.EMA(array(data[day][DataEnum.close][time - (29 + 1):time], dtype=float))
        return self.deleteNanValues(array(emap))

    def getSMAK(self, data, day, time):
        slowk, slowd = talib.STOCH(array(data[day][DataEnum.high][time - (8 + 29 + 3):time], dtype=float),
                                   array(data[day][DataEnum.low][time - (8 + 29 + 3):time], dtype=float),
                                   array(data[day][DataEnum.close][time - (8 + 29 + 3):time], dtype=float))
        slowk = self.deleteNanValues(slowk)
        smak = talib.SMA(slowk)
        return self.deleteNanValues(smak)

    def getSTOCH(self, data, day, time):
        slowk, slowd = talib.STOCH(array(data[day][DataEnum.high][time - (8 + 3):time], dtype=float),
                                   array(data[day][DataEnum.low][time - (8 + 3):time], dtype=float),
                                   array(data[day][DataEnum.close][time - (8 + 3):time], dtype=float))
        return self.deleteNanValues(slowk)

    def getBBANDSUPPER(self, data, day, time):
        upper, middle, lower = talib.BBANDS(array(data[day][DataEnum.close][time - (19 + 1):time], dtype=float),
                                            timeperiod=20)
        return self.deleteNanValues(upper)

    def getBBANDSMIDDLE(self, data, day, time):
        upper, middle, lower = talib.BBANDS(array(data[day][DataEnum.close][time - (19 + 1):time], dtype=float),
                                            timeperiod=20)
        return self.deleteNanValues(middle)

    def getBBANDSLOWER(self, data, day, time):
        upper, middle, lower = talib.BBANDS(array(data[day][DataEnum.close][time - (19 + 1):time], dtype=float),
                                            timeperiod=20)
        return self.deleteNanValues(lower)

    def getPRICE(self, data, day, time):
        price = data[day][DataEnum.close][time]
        return [price]

    def getVOLUME(self, data, day, time):
        volume_ = float(data[day][DataEnum.volume][time])
        return [volume_]

    ##################################################################################
    # past_time -> Overlap Studies Functions
    def getDerivePremiere(self, data, day, time):
        timeT = data[day][DataEnum.close][time]
        timeT1 = data[day][DataEnum.close][time - 1]
        derive = timeT - timeT1
        return [derive]

    def getDeriveSeconde(self, data, day, time):
        timeT = data[day][DataEnum.close][time]
        timeT1 = data[day][DataEnum.close][time - 1]
        timeT2 = data[day][DataEnum.close][time - 2]

        derive1 = timeT - timeT1
        derive2 = timeT1 - timeT2
        deriveSec = derive1 - derive2
        return [deriveSec]

    def getDEMApasttime(self, data, day, time):
        dema = talib.DEMA(array(data[day][DataEnum.volume][time - (58 + 1): time], dtype=float))
        return self.deleteNanValues(dema)

    def getEMAVpasttime(self, data, day, time):
        emav = talib.EMA(array(data[day][DataEnum.volume][time - (29 + 1): time], dtype=float))
        return self.deleteNanValues(emav)

    def getEMAPpasttime(self, data, day, time):
        emap = talib.EMA(array(data[day][DataEnum.close][time - (29 + 1): time], dtype=float))
        return self.deleteNanValues(array(emap))

    def getHT_TRENDLINEpasttime(self, data, day, time):
        ht = talib.HT_TRENDLINE(array(data[day][DataEnum.close][time - (63 + 1): time], dtype=float))
        return self.deleteNanValues(array(ht))

    def getMApasttime(self, data, day, time):
        ma = talib.MA(array(data[day][DataEnum.close][time - (30):time], dtype=float))
        return self.deleteNanValues(array(ma))

    def getMIDPOINTpasttime(self, data, day, time):
        real = talib.MIDPOINT(array(data[day][DataEnum.close][time - (13 + 1): time], dtype=float))
        return self.deleteNanValues(array(real))

    def getMIDPRICEpasttime(self, data, day, time):
        real = talib.MIDPRICE(array(data[day][DataEnum.high][time - (13 + 1): time], dtype=float),
                              array(data[day][DataEnum.low][time - (13 + 1):time], dtype=float))
        return self.deleteNanValues(array(real))

    def getSARpasttime(self, data, day, time):
        sar = talib.SAR(array(data[day][DataEnum.high][time - (1 + 1): time]),
                        array(data[day][DataEnum.low][time - (1 + 1):time]))
        return self.deleteNanValues(sar)

    def getSAREXTpasttime(self, data, day, time):
        sar = talib.SAREXT(array(data[day][DataEnum.high][time - (1 + 1): time]),
                           array(data[day][DataEnum.low][time - (1 + 1):time]))
        return self.deleteNanValues(sar)

    def getT3pasttime(self, data, day, time):
        t3 = talib.T3(array(data[day][DataEnum.close][time - (24 + 1): time]))
        return self.deleteNanValues(t3)

    def getTEMApasttime(self, data, day, time):
        real = talib.T3(array(data[day][DataEnum.close][time - (24 + 1): time]))
        return self.deleteNanValues(real)

    def getTRIMApasttime(self, data, day, time):
        real = talib.TRIMA(array(data[day][DataEnum.close][time - (29 + 1): time]))
        return self.deleteNanValues(real)

    def getWMApasttime(self, data, day, time):
        real = talib.WMA(array(data[day][DataEnum.close][time - (29 + 1): time]))
        return self.deleteNanValues(real)

    ##################################################################################
    # past_time -> Momentum Indicator Functions
    def getADXpasttime(self, data, day, time):
        adx = talib.ADX(array(data[day][DataEnum.high][time - (27 + 1): time], dtype=float),
                        array(data[day][DataEnum.low][time - (27 + 1): time], dtype=float),
                        array(data[day][DataEnum.close][time - (27 + 1): time], dtype=float))
        return self.deleteNanValues(adx)

    def getADXRpasttime(self, data, day, time):
        real = talib.ADXR(array(data[day][DataEnum.high][time - (40 + 1): time], dtype=float),
                          array(data[day][DataEnum.low][time - (40 + 1): time], dtype=float),
                          array(data[day][DataEnum.close][time - (40 + 1): time], dtype=float))
        return self.deleteNanValues(real)

    def getAPOpasttime(self, data, day, time):
        real = talib.APO(array(data[day][DataEnum.close][time - (25 + 1): time], dtype=float))
        return self.deleteNanValues(real)

    def getAROONOSCpasttime(self, data, day, time):
        real = talib.AROONOSC(array(data[day][DataEnum.high][time - (14 + 1): time], dtype=float),
                              array(data[day][DataEnum.low][time - (14 + 1): time], dtype=float))
        return self.deleteNanValues(real)

    def getBOPpasttime(self, data, day, time):
        real = talib.BOP(array(data[day][DataEnum.open_][time - (0 + 1): time], dtype=float),
                         array(data[day][DataEnum.high][time - (0 + 1): time], dtype=float),
                         array(data[day][DataEnum.low][time - (0 + 1): time], dtype=float),
                         array(data[day][DataEnum.close][time - (0 + 1): time], dtype=float))
        return self.deleteNanValues(real)

    def getCCIpasttime(self, data, day, time):
        real = talib.CCI(array(data[day][DataEnum.high][time - (13 + 1): time], dtype=float),
                         array(data[day][DataEnum.low][time - (13 + 1): time], dtype=float),
                         array(data[day][DataEnum.close][time - (13 + 1): time], dtype=float))
        return self.deleteNanValues(real)

    def getCMOpasttime(self, data, day, time):
        real = talib.CMO(array(data[day][DataEnum.close][time - (14 + 1): time], dtype=float))
        return self.deleteNanValues(real)

    def getDXpasttime(self, data, day, time):
        real = talib.DX(array(data[day][DataEnum.high][time - (14 + 1): time], dtype=float),
                        array(data[day][DataEnum.low][time - (14 + 1): time], dtype=float),
                        array(data[day][DataEnum.close][time - (14 + 1): time], dtype=float))
        return self.deleteNanValues(real)

    def getMFIpasttime(self, data, day, time):
        real = talib.MFI(array(data[day][DataEnum.high][time - (14 + 1): time], dtype=float),
                         array(data[day][DataEnum.low][time - (14 + 1): time], dtype=float),
                         array(data[day][DataEnum.close][time - (14 + 1): time], dtype=float),
                         array(data[day][DataEnum.volume][time - (14 + 1): time], dtype=float))
        return self.deleteNanValues(real)

    def getMINUS_DIpasttime(self, data, day, time):
        real = talib.MINUS_DI(array(data[day][DataEnum.high][time - (14 + 1): time], dtype=float),
                              array(data[day][DataEnum.low][time - (14 + 1): time], dtype=float),
                              array(data[day][DataEnum.close][time - (14 + 1): time], dtype=float))
        return self.deleteNanValues(real)

    def getMINUS_DMpasttime(self, data, day, time):
        real = talib.MINUS_DM(array(data[day][DataEnum.high][time - (13 + 1): time], dtype=float),
                              array(data[day][DataEnum.low][time - (13 + 1): time], dtype=float))
        return self.deleteNanValues(real)

    def getMOMpasttime(self, data, day, time):
        real = talib.MOM(array(data[day][DataEnum.close][time - (10 + 1): time], dtype=float))
        return self.deleteNanValues(real)

    def getPLUS_DIpasttime(self, data, day, time):
        real = talib.PLUS_DI(array(data[day][DataEnum.high][time - (14 + 1): time], dtype=float),
                             array(data[day][DataEnum.low][time - (14 + 1): time], dtype=float),
                             array(data[day][DataEnum.close][time - (14 + 1): time], dtype=float))
        return self.deleteNanValues(real)

    def getPLUS_DMpasttime(self, data, day, time):
        real = talib.PLUS_DM(array(data[day][DataEnum.high][time - (13 + 1): time], dtype=float),
                             array(data[day][DataEnum.low][time - (13 + 1): time], dtype=float))
        return self.deleteNanValues(real)

    def getPPOpasttime(self, data, day, time):
        real = talib.PPO(array(data[day][DataEnum.close][time - (25 + 1): time], dtype=float))
        return self.deleteNanValues(real)

    def getROCpasttime(self, data, day, time):
        real = talib.ROC(array(data[day][DataEnum.close][time - (10 + 1): time], dtype=float))
        return self.deleteNanValues(real)

    def getROCPpasttime(self, data, day, time):
        real = talib.ROCP(array(data[day][DataEnum.close][time - (10 + 1): time], dtype=float))
        return self.deleteNanValues(real)

    def getROCRpasttime(self, data, day, time):
        real = talib.ROCR(array(data[day][DataEnum.close][time - (10 + 1): time], dtype=float))
        return self.deleteNanValues(real)

    def getROCR100pasttime(self, data, day, time):
        real = talib.ROCR100(array(data[day][DataEnum.close][time - (10 + 1): time], dtype=float))
        return self.deleteNanValues(real)

    def getRSIpasttime(self, data, day, time):
        real = talib.RSI(array(data[day][DataEnum.close][time - (14 + 1): time], dtype=float))
        return self.deleteNanValues(real)

    # 8 = NAN de stoch / 3 = number of values needed
    def getSTOCHpasttime(self, data, day, time):
        slowk, slowd = talib.STOCH(array(data[day][DataEnum.high][time - (8 + 1): time], dtype=float),
                                   array(data[day][DataEnum.low][time - (8 + 1): time], dtype=float),
                                   array(data[day][DataEnum.close][time - (8 + 1): time], dtype=float))
        return concatenate([self.deleteNanValues(slowk), self.deleteNanValues(slowk)])

    # 8 = NAN de STOCH / 29 = NAN de SMA / 3 = number of values needed
    def getSMAKpasttime(self, data, day, time):
        slowk, slowd = talib.STOCH(array(data[day][DataEnum.high][time - (8 + 29 + 1): time], dtype=float),
                                   array(data[day][DataEnum.low][time - (8 + 29 + 1): time], dtype=float),
                                   array(data[day][DataEnum.close][time - (8 + 29 + 1): time], dtype=float))
        slowk = self.deleteNanValues(slowk)
        smak = talib.SMA(slowk)
        return self.deleteNanValues(smak)

    def getTRIXpasttime(self, data, day, time):
        real = talib.TRIX(array(data[day][DataEnum.close][time - (88 + 1): time], dtype=float))

        return self.deleteNanValues(real)

    def getULTOSCpasttime(self, data, day, time):
        real = talib.ULTOSC(array(data[day][DataEnum.high][time - (28 + 1): time], dtype=float),
                            array(data[day][DataEnum.low][time - (28 + 1): time], dtype=float),
                            array(data[day][DataEnum.close][time - (28 + 1): time], dtype=float))
        return self.deleteNanValues(real)

    def getWILLRpasttime(self, data, day, time):
        real = talib.WILLR(array(data[day][DataEnum.high][time - (13 + 1): time], dtype=float),
                           array(data[day][DataEnum.low][time - (13 + 1): time], dtype=float),
                           array(data[day][DataEnum.close][time - (13 + 1): time], dtype=float))
        return self.deleteNanValues(real)

    ##################################################################################
    # past_time -> Volume Indicator Functions
    def getADpasttime(self, data, day, time):
        real = talib.AD(array(data[day][DataEnum.high][time - (0 + 1): time], dtype=float),
                        array(data[day][DataEnum.low][time - (0 + 1): time], dtype=float),
                        array(data[day][DataEnum.close][time - (0 + 1): time], dtype=float),
                        array(data[day][DataEnum.volume][time - (0 + 1): time], dtype=float))
        return self.deleteNanValues(real)

    def getADOSCpasttime(self, data, day, time):
        real = talib.ADOSC(array(data[day][DataEnum.high][time - (9 + 1): time], dtype=float),
                           array(data[day][DataEnum.low][time - (9 + 1): time], dtype=float),
                           array(data[day][DataEnum.close][time - (9 + 1): time], dtype=float),
                           array(data[day][DataEnum.volume][time - (9 + 1): time], dtype=float))
        return self.deleteNanValues(real)

    ##################################################################################
    # past_time -> Volatility Indicator Functions
    def getATRpasttime(self, data, day, time):
        real = talib.ATR(array(data[day][DataEnum.high][time - (14 + 1): time], dtype=float),
                         array(data[day][DataEnum.low][time - (14 + 1): time], dtype=float),
                         array(data[day][DataEnum.close][time - (14 + 1): time], dtype=float))
        return self.deleteNanValues(real)

    def getNATRpasttime(self, data, day, time):
        real = talib.NATR(array(data[day][DataEnum.high][time - (14 + 1): time], dtype=float),
                          array(data[day][DataEnum.low][time - (14 + 1): time], dtype=float),
                          array(data[day][DataEnum.close][time - (14 + 1): time], dtype=float))
        return self.deleteNanValues(real)

    def getTRANGEpasttime(self, data, day, time):
        real = talib.TRANGE(array(data[day][DataEnum.high][time - (1 + 1): time], dtype=float),
                            array(data[day][DataEnum.low][time - (1 + 1): time], dtype=float),
                            array(data[day][DataEnum.close][time - (1 + 1): time], dtype=float))
        return self.deleteNanValues(real)

    ##################################################################################
    # past_time -> Cycle Indicator Functions
    def getHT_DCPERIODpasttime(self, data, day, time):
        real = talib.HT_DCPERIOD(array(data[day][DataEnum.close][time - (32 + 1): time], dtype=float))
        return self.deleteNanValues(real)

    def getHT_DCPHASEpasttime(self, data, day, time):
        real = talib.HT_DCPERIOD(array(data[day][DataEnum.close][time - (32 + 1): time], dtype=float))
        return self.deleteNanValues(real)

    def getHT_PHASORpasttime(self, data, day, time):
        real = talib.HT_DCPERIOD(array(data[day][DataEnum.close][time - (32 + 1): time], dtype=float))
        return self.deleteNanValues(real)

    def getHT_SINEpasttime(self, data, day, time):
        real = talib.HT_DCPERIOD(array(data[day][DataEnum.close][time - (32 + 1): time], dtype=float))
        return self.deleteNanValues(real)
