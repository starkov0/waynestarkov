__author__ = 'pierrestarkov'
from DataEnum import DataEnum
from datetime import datetime


class DataReader:
    # read list of symbol
    # reads the names of the indexes from a file and saves it in companiesList
    def readCompaniesList(self, fileName):
        with open(fileName) as f:
            filecontent = f.readlines()
            companiesList = []
            for companyName in filecontent:
                companiesList.append(companyName.rstrip('\n\r '))
        return companiesList


    #load data
    def readCompanyDataPerDay(self, companyFile):
        date, time, open_, high, low, close, volume = self.readCompanyData(companyFile)
        dayData = self.separateDataPerDay(date, time, open_, high, low, close, volume)
        return dayData


    # load && append a CSV file
    def readCompanyData(self, companyFile):
        # init
        date, time, open_, high, low, close, volume = ([] for _ in range(7))
        # read
        for line in open(companyFile, 'r'):
            line_ds, line_ts, line_open_, line_high, line_low, line_close, line_volume = line.rstrip().split(',')
            line_dt = datetime.strptime(line_ds + ' ' + line_ts, DataEnum.DATE_FMT + ' ' + DataEnum.TIME_FMT)
            # append
            date.append(line_dt.date())
            time.append(line_dt.time())
            open_.append(float(line_open_))
            high.append(float(line_high))
            low.append(float(line_low))
            close.append(float(line_close))
            volume.append(int(line_volume))

        return date, time, open_, high, low, close, volume


    # separate data per week
    # this function groupes days of one week by comparing variables day1 and day2
    # if day1 if smaller than day2, then day2 is still in the same week day as day1
    # otherwise a new week starts
    def separateDataPerDay(self, date, time, open_, high, low, close, volume):
        dayData = []
        # adding the first date
        day1 = int(date[0].strftime('%u'))
        # create lists
        week_date, week_time, week_open, week_high, week_low, week_close, week_volume = ([] for _ in range(7))
        self.appendWeekDataLists(week_date, week_time, week_open, week_high, week_low, week_close, week_volume,
                                         0, date, time, open_, high, low, close, volume)
        #
        for i in xrange(1, len(date)):
            day2 = int(date[i].strftime('%u'))
            if day1 != day2:  # check if new week starts
                #
                self.appendWeekDataList(dayData, week_date, week_time, week_open,
                                        week_high, week_low, week_close, week_volume)
                #
                week_date, week_time, week_open, week_high, week_low, week_close, week_volume = ([] for _ in range(7))

                self.appendWeekDataLists(week_date, week_time, week_open, week_high, week_low, week_close, week_volume,
                                         i, date, time, open_, high, low, close, volume)

            else:
                self.appendWeekDataLists(week_date, week_time, week_open, week_high, week_low, week_close, week_volume,
                                         i, date, time, open_, high, low, close, volume)
            day1 = day2  # change day1 to day2
            #
        self.appendWeekDataList(dayData, week_date, week_time, week_open,
                                        week_high, week_low, week_close, week_volume)
        return dayData

    def appendWeekDataLists(self, week_date, week_time, week_open, week_high, week_low, week_close, week_volume, i,
                            date, time, open_, high, low, close, volume):
        week_date.append(date[i])
        week_time.append(time[i])
        week_open.append(open_[i])
        week_high.append(high[i])
        week_low.append(low[i])
        week_close.append(close[i])
        week_volume.append(volume[i])

    def appendWeekDataList(self, dayData, week_date, week_time, week_open, week_high, week_low, week_close, week_volume):
        dayData.append([week_date, week_time, week_open, week_high, week_low, week_close, week_volume])
