__author__ = 'pierrestarkov'

from numpy import array, insert
from sklearn.naive_bayes import GaussianNB

class NaiveBayesController():

    def __init__(self):
        self.clf = None

    def train(self,attributes,classes):
        self.clf = GaussianNB()
        self.clf.fit(attributes, classes)

    def test(self,attributes):
        classes = self.clf.predict(attributes)
        return classes
