__author__ = 'pierrestarkov'

from sklearn import svm
from numpy import array, insert

class SVMRegressionController():

    def __init__(self):
        self.clf = None

    def train(self,attributes,classes):
        self.clf = svm.SVR()
        self.clf.fit(attributes, classes)

    def test(self,attributes):
        classes = self.clf.predict(attributes)
        return classes
