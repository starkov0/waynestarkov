__author__ = 'pierrestarkov'
from DataPackage.DataController import DataController
from RegressionPackage  import SVMRegressionController, NaiveBayesController
import matplotlib.pyplot as plt
import time

class MainController():

    def __init__(self):
        self.dataController = DataController()
        # self.predictionController = SVMRegressionController.SVMRegressionController()
        self.predictionController = NaiveBayesController.NaiveBayesController()

    def setStocksData(self,stockIndex):
        start_time = time.time()
        self.dataController.setStockData(stockIndex=stockIndex)
        print("--- time to load stocks data: %s seconds ---" % (time.time() - start_time))

    def prepareAttributes(self):
        start_time = time.time()
        self.attributes = self.dataController.getAttributesTotalMatrix()
        print("--- time to create attributes: %s seconds ---" % (time.time() - start_time))

    def prepareClasses(self,futurDistance):
        start_time = time.time()
        self.classes = self.dataController.getClassesTotalVector(futurDistance=futurDistance)
        print("--- time to create attributes: %s seconds ---" % (time.time() - start_time))

    def predict(self, futurDistance):
        start_time_total = time.time()
        start_time = time.time()
        attributes = self.dataController.dataContainer.attributes
        classes = self.dataController.dataContainer.classes
        twoThird = len(classes)*2/3
        trainAttributes = attributes[0:twoThird]
        trainClasses = classes[0:twoThird]
        testAttributes = attributes[twoThird+1:len(attributes)]
        testClasses = classes[twoThird+1:len(classes)]
        print("--- time to separate sets: %s seconds ---" % (time.time() - start_time))

        start_time = time.time()
        self.predictionController.train(attributes=trainAttributes, classes=trainClasses)
        print("--- time to train: %s seconds ---" % (time.time() - start_time))

        start_time = time.time()
        predictions = self.predictionController.test(attributes=testAttributes)
        print("--- time to test: %s seconds ---" % (time.time() - start_time))
        print("--- total time to predict: %s seconds ---" % (time.time() - start_time_total))

        fig = plt.figure()
        title = self.dataController.dataContainer.stockSymbol + ' : NO transalation : prediction with futur time : ' + str(futurDistance)
        fig.suptitle(title, fontsize=20)
        plt.plot(predictions[0:1100], '-r')
        plt.plot(testClasses[0:1100], '-b')
        fig.savefig('../Plots/' + title + '.jpg')



main = MainController()
for i in xrange(3,803,100):
    main.setStocksData(i)
    main.prepareAttributes()
    for futurDistance in [20]:
        main.prepareClasses(futurDistance=futurDistance)
        main.predict(futurDistance=futurDistance)